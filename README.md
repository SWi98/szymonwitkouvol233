# README #

I've skipped the Newton's Cradle and managed to implement most of the Towers. 

Object pooling in Towers seems to be a natural thing to do. We deal with a big number of bullets so destroying and instantiating them can become costly. I didn't implement the pooling - I've'never done it before, so it would probably take a lot longer than I had for this task. 

### What's still TODO in the Towers? ###

* Set the speed of bullets for 4 units/second. 
* Changing the towers behaviour after reaching 100 towers.