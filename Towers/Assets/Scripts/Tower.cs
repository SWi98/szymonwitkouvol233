﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{

    public GameObject bulletPrefab;

    private int shootCount;
    private bool active;
    private GameObject firedBall;
    private SpriteRenderer sprite;

    private void Awake()
    {
        shootCount = 0;
        InvokeRepeating(nameof(DoTurn), 0.5f, 0.5f);
        sprite = GetComponent<SpriteRenderer>();
        sprite.color = new Color(1, 0, 0, 1);
        active = true;
    }

    private void DoTurn()
    {
        if (active)
        {
            Rotate();
            Shoot();
        }
        if(shootCount == 12)
        {
            sprite.color = new Color(1, 1, 1, 1);
            active = false;
        }
    }

    public void OnInstantiate()
    {
        active = false;
        StartCoroutine(WaitAndStart(6f));
    }

    IEnumerator WaitAndStart(float duration)
    {
        yield return new WaitForSeconds(duration);   //Wait
        active = true;
    }


    private void Rotate()
    {
        int rotateValue = Random.Range(15, 45);
        transform.Rotate(new Vector3(0, 0, rotateValue));
    }

    private void Shoot()
    {
        shootCount++;
        firedBall = Instantiate(bulletPrefab, transform.position - new Vector3(0, 0, 1), this.transform.rotation);

    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Bullet" && other.gameObject != firedBall)
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
    }
}
