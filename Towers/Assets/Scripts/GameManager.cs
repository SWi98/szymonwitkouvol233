﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject towersTextGO;
    private TextMeshProUGUI towersText;

    private void Awake()
    {
        instance = this;
        towersText = towersTextGO.GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        
    }

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                Debug.LogError("Missing GameManager");
            }
            return instance;
        }
    }

    private void Update()
    {
        GameObject[] towers; 
        towers = GameObject.FindGameObjectsWithTag("Tower");
        if(towers.Length > 100)
        {
            // All towers should shoot, none should be instantiated 

        }

        towersText.text = "Towers:" + towers.Length;

    }


}
