﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject towerPrefab; 

    private int distance;
    private Vector3 destination;
    private float interpolate;
    void Awake()
    {
        distance = Random.Range(1, 4);
        destination = transform.right * distance;
        interpolate = 0;
        //transform.Translate(transform.right * distance);
    }

    private void Update()
    {
        //transform.Translate(transform.right * Time.deltaTime);
        if (transform.position == destination)
        {
            Destroy(this.gameObject);
            GameObject newTower = Instantiate(towerPrefab, transform.position, towerPrefab.transform.rotation);
            newTower.GetComponent<Tower>().OnInstantiate();
        }
        else
        {
            interpolate += Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, destination, interpolate);
        }
    }

}
